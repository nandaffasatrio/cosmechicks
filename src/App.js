import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.scss";
// import Card from "./components/card";
// import Hero from "./components/Hero";
import Header from "./components/Header";
import LoginPage from "./pages/LoginPage";
import Registration from "./pages/Registration";
import ProductDetail from "./pages/ProductDetail";
import ProductLanding from "./pages/ProductLanding";
import Checkout from "./pages/Checkout";
import Homepage from "./pages/Homepage";
import Footer from "./components/Footer";
import MyCart from "./pages/MyCart";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/loginpage" element={<LoginPage />} />
          <Route path="/registration" element={<Registration />} />
          <Route path="/product-detail/:id" element={<ProductDetail />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/productlanding" element={<ProductLanding />} />
          <Route path="/mycart" element={<MyCart />} />
        </Routes>
        {/* <Hero />
        <Card /> */}
        <Footer />
      </div>
    </Router>
  );
}

export default App;
