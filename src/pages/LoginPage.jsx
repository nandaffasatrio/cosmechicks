import React from "react";
import alertify from "alertifyjs";
// import { Validation } from "../services/Validation";
import { useNavigate } from "react-router-dom";

class LoginPage extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
    };
  }

  handleSubmit = (e) => {
    const users = JSON.parse(localStorage.getItem("users"));
    let userFound = null;
    users.forEach((user) => {
      if (user.email === this.state.email) {
        userFound = user;
      }
    });
    if (userFound) {
      if (userFound.password !== this.state.password) {
        alertify.alert("Login Failed", "You Have Submit Wrong Password");
      } else {
        localStorage.setItem("isAuthenticated", true);
        localStorage.setItem("userLogin", userFound.email);
        this.props.navigate("/");
        window.location.reload();
      }
    } else {
      alertify.alert("Login gagal", "Email tidak terdaftar");
    }
  };
  // loadFromStorage = (data) => {
  //   if (!localStorage.getItem("users")) {
  //     localStorage.setItem("users", JSON.stringify([]));
  //   }
  //   let users = JSON.parse(localStorage.getItem("users"));
  //   let newUser = {
  //     name: data.name,
  //     email: data.email,
  //     password: data.password,
  //   };
  //   users.push(newUser);
  //   localStorage.setItem("users", JSON.stringify(users));
  //   this.props.navigate("/LoginPage");
  // };
  render() {
    return (
      <div className="LoginPage">
        <div className="container container-login">
          <div className="card card-login mx-auto">
            <h2 className="text-center mt-4">Customer Login</h2>
            <form className="row g-4 needs-validation mx-auto form-login">
              <div className="col-12">
                <label htmlFor="email" className="form-label">
                  Email address
                </label>
                <input type="email" className="form-control" onChange={(e) => this.setState({ email: e.target.value })} id="email" placeholder="name@example.com" />
                <p className="text-danger"></p>
              </div>
              <div className="col-12">
                <label htmlFor="password" className="form-label">
                  Password
                </label>
                <input type="password" onChange={(e) => this.setState({ password: e.target.value })} id="password" className="form-control" aria-describedby="passwordHelpBlock" />
                <p className="text-danger"></p>
              </div>
              <button className="btn btn-lg btn-primary btn-next" onClick={this.handleSubmit} type="button">
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
const LoginNavigate = () => {
  const navigate = useNavigate();
  return <LoginPage navigate={navigate} />;
};

export default LoginNavigate;
