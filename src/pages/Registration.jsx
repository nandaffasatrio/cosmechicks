import alertify from "alertifyjs";
import React from "react";
import { Validation } from "../services/Validation";
import { useNavigate } from "react-router-dom";
// import alertify from "alertifyjs";

class Registration extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      birth: "",
      email: "",
      phone: "",
      address: "",
      province: "",
      city: "",
      zip: "",
      password: "",
      cpassword: "",
      terms: false,
    };
  }
  handleNext = (e) => {
    const formPage = e.target.parentElement.parentElement.parentElement.parentElement;
    const nextPage = formPage.nextElementSibling;
    formPage.classList.add("d-none");
    nextPage.classList.remove("d-none");
  };
  handlePrev = (e) => {
    const formPage = e.target.parentElement.parentElement.parentElement.parentElement;
    const prevPage = formPage.previousElementSibling;
    formPage.classList.add("d-none");
    prevPage.classList.remove("d-none");
  };
  handleSubmit = (e) => {
    console.log("register");
    const nameRequired = Validation.required("name", this.state.name, "Name Must Be Filled");
    const dateOfBirthRequired = Validation.required("birth", this.state.birth, "Date of Birth Must Be Filled");
    const emailRequired = Validation.required("email", this.state.email, "Email Must Be Filled");
    const checkRequired = Validation.required("terms", this.state.terms, "You Must Agree with Terms");
    const PhoneRequired = Validation.required("phone", this.state.phone, "Phone Number Must Be Filled");
    const addressRequired = Validation.required("address", this.state.address, "Please Enter Your Address");
    const provinceRequired = Validation.required("province", this.state.province, "Province Must Be Choosen");
    const cityRequired = Validation.required("city", this.state.city, "City Must Be Choosen");
    const zipRequired = Validation.required("zip", this.state.zip, "Please Enter ZIP Code");
    const passwordRequired = Validation.required("password", this.state.password, "Please Enter Your Password");
    const cpasswordRequired = Validation.required("cpassword", this.state.cpassword, "Please Enter Your Confirm Password");
    const termsRequired = Validation.isChecked("terms", this.state.terms, "Agree With terms And Condition");
    // const passwordEqual = Validation.isEqual("password", [this.state.password, this.state.cpassword], "Password Werent Same");
    let passwordEqual = true;
    if (passwordRequired && cpasswordRequired) {
      passwordEqual = Validation.isEqual("password", [this.state.password, this.state.cpassword], "Password Werent Same");
    }
    const allValidation =
      nameRequired && emailRequired && dateOfBirthRequired && PhoneRequired && checkRequired && addressRequired && provinceRequired && cityRequired && zipRequired && passwordRequired && cpasswordRequired && passwordEqual && termsRequired;
    if (!allValidation) {
      alertify.alert("Validasi Gagal", "Lengkapi Semua Input");
    } else {
      this.saveToStorage({
        name: this.state.name,
        email: this.state.email,
        Birth: this.state.Birth,
        phone: this.state.phone,
        address: this.state.address,
        province: this.state.province,
        city: this.state.city,
        zip: this.state.zip,
        password: this.state.password,
      });
    }
  };

  saveToStorage = (data) => {
    if (!localStorage.getItem("users")) {
      localStorage.setItem("users", JSON.stringify([]));
    }
    let users = JSON.parse(localStorage.getItem("users"));
    let newUser = {
      name: data.name,
      email: data.email,
      Birth: data.Birth,
      phone: data.phone,
      address: data.address,
      province: data.province,
      city: data.city,
      password: data.password,
      carts: [],
    };
    users.push(newUser);
    localStorage.setItem("users", JSON.stringify(users));
    this.props.navigate("/LoginPage");
  };

  render() {
    return (
      <div className="registration">
        <div className="container container-registration">
          <div className="card card-registration mx-auto">
            <div className="progress mx-auto" style={{ width: "100%" }}>
              <div className="progress-bar progress-bar-stiped progress-bar-animated" role="progressbar" style={{ width: "30%" }}></div>
            </div>
            <form className="row g-4 needs-validation">
              <div className="col-6">
                <label htmlFor="name" className="form-label">
                  Name
                </label>
                {/* merubah apapun state yg kita input, tidak lagi memaki value */}
                <input type="text" className="form-control" id="name" onChange={(e) => this.setState({ name: e.target.value })} required />
                <p className="text-danger"></p>
              </div>
              <div className="col-12">
                <label htmlFor="birth" className="form-label">
                  Date of Birth
                </label>
                <input type="date" className="form-control" id="birth" aria-describedby="inputGroupPrepend" onChange={(e) => this.setState({ birth: e.target.value })} required />
                <p className="text-danger"></p>
              </div>
              <div className="col-12">
                <label htmlFor="email" className="form-label">
                  Email
                </label>
                <input type="email" className="form-control" id="email" onChange={(e) => this.setState({ email: e.target.value })} required />
                <p className="text-danger"></p>
              </div>
              <div className="col-12">
                <label htmlFor="phone" className="form-label">
                  Phone Number
                </label>
                <input type="text" className="form-control" onChange={(e) => this.setState({ phone: e.target.value })} id="phone" htmlFor="phone" required />
                <p className="text-danger"></p>
              </div>
              <div className="col-12">
                <button className="btn btn-primary d-block mx-auto w-100 btn-next" onClick={this.handleNext} type="button">
                  Next
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="container container-registration d-none">
          <div className="card card-registration mx-auto">
            <div className="progress mx-auto" style={{ width: "100%" }}>
              <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: "70%" }}></div>
            </div>
            <form className="row g-4 needs-validation" noValidate>
              <div className="col-12">
                <label htmlFor="address" className="form-label">
                  Address
                </label>
                <input type="address" height="100px" className="form-control" id="address" onChange={(e) => this.setState({ address: e.target.value })} aria-describedby="inputGroupPrepend" required />
                <p className="text-danger"></p>
              </div>
              <div className="col-6">
                <label htmlFor="province" className="form-label">
                  Province
                </label>
                <select className="form-select" onChange={(e) => this.setState({ province: e.target.value })} id="province" required>
                  <option selected disabled>
                    Choose...
                  </option>
                  <option>DKI Jakarta</option>
                  <option>Jawa Barat</option>
                  <option>Jawa Timur</option>
                  <option>Jawa Tengah</option>
                </select>
                <p className="text-danger"></p>
              </div>
              <div className="col-6">
                <label htmlFor="city" className="form-label">
                  City
                </label>
                <select className="form-select" onChange={(e) => this.setState({ city: e.target.value })} id="city" required>
                  <option selected disabled>
                    Choose...
                  </option>
                  <option>Jakarta</option>
                  <option>Bandung</option>
                  <option>Brebes</option>
                  <option>Bogor</option>
                  <option>Surabaya</option>
                </select>
                <p className="text-danger"></p>
              </div>
              <div className="row">
                <div className="col-6 offset-3 justify-content-center mx-auto mb-3 mt-3">
                  <label htmlFor="validationCustom05" className="form-label">
                    Zip
                  </label>
                  <input type="text" onChange={(e) => this.setState({ zip: e.target.value })} className="form-control" id="zip" required />
                  <p className="text-danger"></p>
                </div>
              </div>
              <div className="col-12 mx-auto flex-column d-flex justify-content-center">
                <button className="btn btn-primary mb-2 btn-prev" onClick={this.handlePrev} type="button">
                  Previous
                </button>
                <button className="btn btn-primary btn-next" onClick={this.handleNext} type="button">
                  Next
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="container container-registration d-none">
          <div className="card card-registration mx-auto">
            <div className="progress mx-auto" style={{ width: "100%" }}>
              <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: "100%" }}></div>
            </div>
            <form className="row g-4">
              <div className="col-12">
                <label htmlFor="password" className="col-sm-2 col-form-label">
                  Password
                </label>
                <div className="col-12">
                  <input type="password" onChange={(e) => this.setState({ password: e.target.value })} className="form-control" id="password" />
                  <p className="text-danger"></p>
                </div>
              </div>
              <div className="col-12">
                <label htmlFor="cpassword" className="col-sm-2 col-form-label">
                  Confirm Password
                </label>
                <div className="col-12">
                  <input type="password" onChange={(e) => this.setState({ cpassword: e.target.value })} className="form-control" id="cpassword" />
                  <p className="text-danger"></p>
                </div>
              </div>
              <div className="col-12">
                <div className="form-check">
                  <input className="form-check-input" type="checkbox" onChange={(e) => this.setState({ terms: true })} id="terms" required />
                  <label className="form-check-label" htmlFor="terms">
                    Agree to terms and conditions
                  </label>
                  <p className="text-danger"></p>
                </div>
              </div>
              <div className="col-12 offset-3 mx-auto">
                <button className="btn btn-primary mb-2 w-100 d-block btn-prev" onClick={this.handlePrev} type="button">
                  Previous
                </button>
                <button className="col-12 btn btn-primary mx-auto w-100 d-block regsubmit" onClick={this.handleSubmit} type="button">
                  Submit form
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const RegisterNavigate = () => {
  const navigate = useNavigate();
  return <Registration navigate={navigate} />;
};

export default RegisterNavigate;
