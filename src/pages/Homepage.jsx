import React from 'react';
// import React, { Component } from "react";
import Hero from '../components/1.jsx';
// import Card from "../components/Card";
import { Link } from 'react-router-dom';
// import Products from "../data/Products";
import HotCard from '../components/HotCard';
import LatestCard from '../components/LatestCard';
// import {Products} from "../data/Products.js";
import { getTrending, getHottest } from '../services/Filter';

class HomePage extends React.Component {
  // componentDidMount() {
  //   console.log(getHottest);
  // }

  render() {
    return (
      <div className='container p-0'>
        <div className='homepage'>
          <Hero />
          <div className='card_wrapper g-0'>
            {/* <h2 className="text-center">Hottest Product</h2> */}
            <div className='row hottest-row'>
              <div className='col-xl-3 col-lg-3 col-12 mt-5 d-flex align-items-center justify-content-center text-center'>
                <h3 className='text-dark'>Hot Items For You</h3>
              </div>
              <div className='col-xl-9 col-12 col-lg-12 col-md-12 col-sm-12 col-12 py-4'>
                <div className='row'>
                  {getHottest.map((Hottest, index) => {
                    console.log(Hottest.id);
                    return (
                      <React.Fragment key={Hottest.id}>
                        <div className='col-lg-3 col-xl-3 col-md-6 col-sm-12 col-12 mb-4 hottesss'>
                          <HotCard
                            HottestId={Hottest.id}
                            HottestName={Hottest.name}
                            HottestPrice={Hottest.price}
                            HottestImage={Hottest.image[0]}
                          />
                        </div>
                      </React.Fragment>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className='container-fluid  mb-5'>
              <div className='row row-thinks mb-5'>
                <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-6 think-high'>
                  <div className='row d-flex flex-column align-items-center'>
                    <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-6 mt-3'>
                      <p>Think high always espect the best.</p>
                    </div>
                    <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-6 made-by'>
                      {' '}
                      <p> Made by Love</p>
                    </div>
                  </div>
                </div>
                <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-6 your-skin'>
                  <div className='container text-center'>
                    <p>
                      Your skin is the fingerprint of it what is going on inside
                      your body, and skin conditions, are the manifestations of
                      your body’s internal needs including it’s nutrional needs.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className='row shipping mx-auto'>
              <div className='col-xxl-9 col-xl-9 text-center col-lg-9 col-md-9 col-9 mx-auto'>
                <div className='row row-shipping d-flex'>
                  <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-4 text-center icon-img'>
                    <img
                      alt='truck'
                      src={require('../assets/img/consgoods/truck.png')}
                    />
                    <h5>
                      Shipping Around <br /> The World
                    </h5>
                  </div>
                  <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-4 text-center icon-img'>
                    <img
                      alt='truck'
                      src={require('../assets/img/consgoods/money-back.png')}
                    />
                    <h5>Money Back Guarantee</h5>
                  </div>
                  <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-4 text-center icon-img'>
                    <img
                      alt='truck'
                      src={require('../assets/img/consgoods/free-return.png')}
                    />
                    <h5>
                      Up to 30 Days <br /> Free Return
                    </h5>
                  </div>
                </div>
              </div>
            </div>
            <div className='row hottest-row mt-5'>
              <div className='col-xl-9 col-12'>
                <div className='row ' style={{ margin: '10px' }}>
                  {getTrending.map((Latest, index) => {
                    return (
                      <React.Fragment key={Latest.id}>
                        <div className='col-lg-3 col-md-6 col-sm-12 col-12 mb-4'>
                          <LatestCard
                            LatestId={Latest.id}
                            LatestName={Latest.name}
                            LatestPrice={Latest.price}
                            LatestImage={Latest.image[0]}
                          />
                        </div>
                      </React.Fragment>
                    );
                  })}
                </div>
              </div>
              <div className='col-xl-3 col-lg-0 col-md-0 d-flex justify-content-center align-items-center text-center'>
                <h3 className='text-dark'>Our Latest Items</h3>
              </div>
            </div>
          </div>
          <div className='mb-5'>
            <div className='row'>
              <div className='col-xl-6 col-lg-6 text-center col-md-12 col-sm-12 col-img-women'>
                <img
                  alt='truck'
                  className='img-about'
                  src={require('../assets/img/about.png')}
                />
              </div>
              <div className='col-xl-6 col-lg-6 text-center col-md-0 col-sm-0 d-lg-block d-none d-flex flex-column align-items-center women'>
                <div className='row row-community d-flex flex-column align-items-center'>
                  <div className='col-4 col-community'>
                    <p>A community for every stage of your journey</p>
                  </div>
                  <div className='col-4 col-members'>
                    <p>
                      Our members-only community cheers you on. Remember -
                      result vary from person to person, because no two cases
                      are alike!!!!!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-6 mx-auto mt-5  '>
              <Link to='/productlanding'>
                <button className='d-block w-100 mb-5 btn-baru' type='button'>
                  Show More Product
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HomePage;
