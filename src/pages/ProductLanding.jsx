import React from 'react';
import Card from '../components/Card';
// import { Link } from "react-router-dom";
import Products from '../data/Products';
import { ProductService } from '../services/ProductService';
// import Hottest from "../data/Hottest";

class ProductLanding extends React.Component {
  constructor() {
    super();
    this.state = {
      Products: Products,
    };
  }

  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
  }

  filterByCategory = e => {
    const value = e.target.value;
    this.setState({
      Products: ProductService.filterByCategory(value),
    });
  };

  render() {
    console.log(this.filterByCategory);

    return (
      <div className='productlanding container'>
        <div className='row'>
          <div className='mt-5 d-xl-block d-lg-block d-none col-2 filter-panel'>
            <div className='card-body'>
              <h5 className='card-title'>Category</h5>
              <div className='form-check'>
                <input
                  className='form-check-input'
                  type='radio'
                  name='category'
                  value='men'
                  onChange={this.filterByCategory}
                  id='men'
                />
                <label className='form-check-label' htmlFor='flexCheckDefault'>
                  Men
                </label>
              </div>
              <div className='form-check'>
                <input
                  className='form-check-input'
                  type='radio'
                  name='category'
                  value='women'
                  onChange={this.filterByCategory}
                  id='women'
                />
                <label className='form-check-label' htmlFor='flexCheckDefault'>
                  Women
                </label>
              </div>
              {/* <div className="form-check">
                      <input className="form-check-input" type="radio" name="category" value="women" onChange={this.filterByCategory} id="toner" />
                      <label className="form-check-label" htmlFor="flexCheckDefault">
                        Toner
                      </label>
                    </div> */}
            </div>
            <ul className='list-group list-group-flush'>
              <li className='list-group-item'>
                {' '}
                <div className='form-check'>
                  <input
                    className='form-check-input'
                    type='radio'
                    name='category'
                    value='women'
                    onChange={this.filterByCategory}
                    id='serum'
                  />
                  <label
                    className='form-check-label'
                    htmlFor='flexCheckDefault'
                  >
                    Serum
                  </label>
                </div>
              </li>

              <li className='list-group-item'>
                {' '}
                <div className='form-check'>
                  <input
                    className='form-check-input'
                    type='radio'
                    name='category'
                    value='men'
                    onChange={this.filterByCategory}
                    id='flexCheckDefault'
                  />
                  <label
                    className='form-check-label'
                    htmlFor='flexCheckDefault'
                  >
                    Perfume
                  </label>
                </div>
              </li>
              <li className='list-group-item'>
                {' '}
                <div className='form-check'>
                  <input
                    className='form-check-input'
                    type='radio'
                    name='category'
                    value='men'
                    onChange={this.filterByCategory}
                    id='face-wash'
                  />
                  <label
                    className='form-check-label'
                    htmlFor='flexCheckDefault'
                  >
                    Face Wash
                  </label>
                </div>
              </li>
              <li className='list-group-item'>
                {' '}
                <div className='form-check'>
                  <input
                    className='form-check-input'
                    type='radio'
                    name='category'
                    value='women'
                    onChange={this.filterByCategory}
                    id='makeup'
                  />
                  <label
                    className='form-check-label'
                    htmlFor='flexCheckDefault'
                  >
                    Make Up
                  </label>
                </div>
              </li>
            </ul>
          </div>
          <div className='col-12 col-lg-10 col-md-12 col-sm-12'>
            <div className='row'>
              <div className='card_landing row g-0'>
                <h2 className='text-center mt-3'>This Is Our Product</h2>
                <div className='container row '>
                  <div className='col-sm-12'>
                    <div className='row'>
                      {this.state.Products.map((product, index) => {
                        return (
                          <React.Fragment key={product.id}>
                            <div className='col-12 col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-5'>
                              {/* <Link to={`/product-detail/${product.id}`}> */}
                              <Card
                                productName={product.name}
                                productPrice={product.price}
                                productImage={product.image[0]}
                                id={product.id}
                              />
                              {/* </Link> */}
                            </div>
                          </React.Fragment>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductLanding;
