import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Products } from '../data/Products';
import alertify from 'alertifyjs';
import { numberFormat } from '../services/NumberFormat';

class ProductDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      productDetail: {},
      total: 0,
      quantity: 1,
    };
  }

  componentDidMount() {
    Products.forEach(product => {
      if (product.id === parseInt(this.props.idProduct)) {
        this.setState({
          productDetail: product,
          total: product.price,
        });
      }
    });
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
  }

  handleIncrement = () => {
    this.setState({
      quantity: this.state.quantity + 1,
      total: this.state.total + this.state.productDetail.price,
    });
  };

  handleDecrement = () => {
    if (this.state.quantity === 1) {
      this.setState({
        quantity: 1,
      });
    } else {
      this.setState({
        quantity: this.state.quantity - 1,
        total: this.state.total - this.state.productDetail.price,
      });
    }
  };

  handleAddToCart = () => {
    let userLogin = null;
    const emailLogin = localStorage.getItem('userLogin');

    if (!emailLogin) {
      alertify.alert('Failed', 'Please Login first!!');
    }

    const users = JSON.parse(localStorage.getItem('users'));
    users.forEach(user => {
      if (user.email === emailLogin) {
        userLogin = user;
      }
    });

    // validasi jika id product yang di add itu sama
    let isSame = false;
    userLogin.carts.forEach(item => {
      if (this.state.productDetail.id === item.id) {
        isSame = true;
      }
    });

    let noUrut = 0;

    if (userLogin.carts.length === 0) {
      noUrut = 1;
    } else {
      noUrut = userLogin.carts.slice(-1).pop();
      noUrut = parseInt(noUrut.noUrut) + 1;
    }

    const newItem = {
      id: this.state.productDetail.id,
      quantity: this.state.quantity,
      date: new Date(),
      noUrut: noUrut,
    };

    if (isSame) {
      alertify.alert('Add to cart aborted', 'You already added this product');
    } else {
      userLogin.carts.push(newItem);

      // kita harus dapetin semua user kecuali user yang login
      let userWithoutUserLogin = users.filter(
        user => user.email !== emailLogin
      );
      userWithoutUserLogin.push(userLogin);
      localStorage.setItem('users', JSON.stringify(userWithoutUserLogin));
      alertify
        .confirm(
          'Add to cart Success',
          'This Product has been added',
          () => {
            this.props.navigate('/mycart');
          },
          () => {
            this.props.navigate('/productlanding');
          }
        )
        .set('labels', { ok: 'Go to Cart!', cancel: 'Continue shopping' });
    }

    // push ke cart user yang login
  };

  render() {
    console.log(this.state.productDetail.image);
    return (
      <main className='container py-5'>
        <h1>Product Detail</h1>
        <hr />
        <div className='card mb-3'>
          <div className='row g-0 py-5 px-3'>
            <div className='col-8'>
              <div className='card-body'>
                <div>
                  <h5 className='card-title'>
                    {this.state.productDetail.image ? (
                      <img
                        style={{ width: '200px' }}
                        src={require(`../assets/img/consgoods/${this.state.productDetail.image[0]}`)}
                        alt=''
                      />
                    ) : (
                      ''
                    )}
                  </h5>
                </div>
                <h5 className='card-title'>{this.state.productDetail.name}</h5>
                <h6>{numberFormat(this.state.productDetail.price)}</h6>
                <p className='card-text'>
                  <small className='text-muted'>Last updated 3 mins ago</small>
                </p>
              </div>
            </div>
            <div className='col-4 product-button'>
              <div
                className='input-group mb-3'
                style={{ display: 'flex', justifyContent: 'space-between' }}
              >
                <button
                  className='input-group-text btn btn-primary px-4'
                  onClick={this.handleDecrement}
                >
                  -
                </button>
                <h2>{this.state.quantity}</h2>
                <button
                  className='input-group-text btn btn-primary px-4'
                  onClick={this.handleIncrement}
                >
                  +
                </button>
              </div>
              <span className='mt-3 d-inline-block'>Total Price</span>
              <h4>{numberFormat(this.state.total)}</h4>
              <button
                className='btn btn-primary button'
                onClick={this.handleAddToCart}
              >
                Add to Cart
              </button>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

const ProductDetailParams = () => {
  const params = useParams();
  const navigate = useNavigate();
  const idProduct = params.id;
  return <ProductDetail idProduct={idProduct} navigate={navigate} />;
};

export default ProductDetailParams;
