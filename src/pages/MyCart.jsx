import React from "react";
import { Products } from "../data/Products";
import { Link } from "react-router-dom";

class MyCart extends React.Component {
  constructor() {
    super();
    this.state = {
      total: 0,
      productInCart: [],
    };
  }

  componentDidMount() {
    const emailLogin = localStorage.getItem("userLogin");
    const users = JSON.parse(localStorage.getItem("users"));
    const userLogin = users.filter((user) => user.email === emailLogin)[0];
    let total = 0;
    let productInCart = [];
    userLogin.carts.forEach((cart) => {
      const product = Products.filter((product) => product.id === cart.id)[0];
      total += product.price * cart.quantity;
      product.quantity = cart.quantity;
      product.subTotal = product.price * cart.quantity;
      product.noUrut = cart.noUrut;
      productInCart.push(product);
    });
    this.setState({
      total: total,
      productInCart: productInCart,
    });
  }

  handleIncrement = (id) => {
    let products = this.state.productInCart.filter((product) => product.id !== id);
    const productUpdate = this.state.productInCart.filter((product) => product.id === id)[0];
    productUpdate.quantity += 1;
    productUpdate.subTotal = productUpdate.subTotal + productUpdate.price;
    products.push(productUpdate);
    products = products.sort((a, b) => a.noUrut - b.noUrut);

    // total
    let total = 0;
    products.forEach((product) => {
      total += product.subTotal;
    });

    this.setState({
      total: total,
      productInCart: products,
    });
  };

  handleDecrement = (id) => {
    let products = this.state.productInCart.filter((product) => product.id !== id);
    const productUpdate = this.state.productInCart.filter((product) => product.id === id)[0];
    productUpdate.quantity -= 1;
    productUpdate.subTotal = productUpdate.subTotal - productUpdate.price;
    products.push(productUpdate);
    products = products.sort((a, b) => a.noUrut - b.noUrut);

    // total
    let total = 0;
    products.forEach((product) => {
      total += product.subTotal;
    });

    this.setState({
      total: total,
      productInCart: products,
    });
  };

  render() {
    return (
      <main className="container">
        <h2>My Cart</h2>
        <hr />
        <div className="row">
          <div className="col-7">
            <h3>Products in Cart</h3>
            <hr />
            <div className="row">
              {this.state.productInCart.map((product) => {
                return (
                  <React.Fragment key={product.id}>
                    <div className="col-12">
                      <div className="card">
                        <div className="card-body">
                          <h4>{product.name}</h4>
                          <span>Price Rp {product.price}</span>
                          <div
                            className="input-group"
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                            }}
                          >
                            <button className="input-group-text btn btn-primary" onClick={() => this.handleDecrement(product.id)}>
                              -
                            </button>
                            <h2>{product.quantity}</h2>
                            <button className="input-group-text btn btn-primary" onClick={() => this.handleIncrement(product.id)}>
                              +
                            </button>
                          </div>
                          <span>Sub Total : Rp {product.subTotal}</span>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                );
              })}
            </div>
          </div>
          <div className="col-5">
            <h3>Calculate Total</h3>
            <hr />
            <span>Total : Rp {this.state.total}</span>
            <br />
            <br />
            <Link to="/checkout" className="mt-5 btn btn-primary">
              Proceed To Checkout
            </Link>
          </div>
        </div>
      </main>
    );
  }
}

export default MyCart;
