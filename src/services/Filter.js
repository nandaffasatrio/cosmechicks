import Products from "../data/Products";

// ====TRENDING=====
export const getTrending = Products.filter((e) => {
  return e.status.includes("latest");
});

// ====HOTTEST/LATEST=====
export const getHottest = Products.filter((e) => {
  return e.status.includes("hotest");
});
