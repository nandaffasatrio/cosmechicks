import Products from "../data/Products";

export const ProductService = {
  filterByCategory(name) {
    let products = [];
    Products.forEach((product) => {
      product.category.forEach((ct) => {
        if (ct === name) {
          products.push(product);
        }
      });
    });
    return products;
  },
};
