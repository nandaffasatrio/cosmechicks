import React from "react";
import { numberFormat } from "../services/NumberFormat";
import { Link } from "react-router-dom";

const Card = (props) => {
  return (
    <div className="card bg-light mt-5 text-center cards-main">
      <img className="rounded text-center" src={require(`../assets/img/consgoods/${props.productImage}`)} alt="" />
      <div className="card-body">
        <p className="card-title">{props.productName}</p>
        <p className="card-text card-price">{numberFormat(props.productPrice)}</p>

        <Link to={`/product-detail/${props.id}`}>
          {" "}
          <button href="/" className="add-cart text-center mx-auto">
            Quick View
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Card;
