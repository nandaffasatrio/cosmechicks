import React from 'react';
import { numberFormat } from '../services/NumberFormat';
import { Link } from 'react-router-dom';

const LatestCard = props => {
  return (
    <div className='card text-center cards-main' style={{ width: '100%' }}>
      <img
        src={require(`../assets/img/consgoods/${props.LatestImage}`)}
        alt=''
      />
      <div className='card-body'>
        <p className='card-title'>{props.LatestName}</p>
        <p className='card-text card-price'>
          {numberFormat(props.LatestPrice)}
        </p>
        {/* <button className="add-cart ">Add To Cart</button> */}
        <Link to={`/product-detail/${props.LatestId}`}>
          {' '}
          <button href='/' className='add-cart text-center mx-auto'>
            Quick View
          </button>
        </Link>
      </div>
    </div>
  );
};

export default LatestCard;
