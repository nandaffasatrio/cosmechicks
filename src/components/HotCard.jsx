import React from 'react';
import { numberFormat } from '../services/NumberFormat';
import { Link } from 'react-router-dom';

const HotCard = props => {
  return (
    <div
      className='card text-center cards-main'
      style={{ marginRight: '0', width: '100%' }}
    >
      <img
        src={require(`../assets/img/consgoods/${props.HottestImage}`)}
        alt=''
      />
      <div className='card-body'>
        <p className='card-title'>{props.HottestName}</p>
        <p className='card-text card-price'>
          {numberFormat(props.HottestPrice)}
        </p>
        {/* <button className="add-cart">Add To Cart</button> */}
        <Link to={`/product-detail/${props.HottestId}`}>
          {' '}
          <button href='/' className='add-cart text-center mx-auto'>
            Quick View
          </button>
        </Link>
      </div>
    </div>
  );
};

export default HotCard;
