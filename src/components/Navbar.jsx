// import { Button } from "bootstrap";
import React from 'react';
import { Link } from 'react-router-dom';
import cart from '../assets/img/cart.png';
import notif from '../assets/img/notif.png';
import profile from '../assets/img/profile.png';
// import LoginPage from "../pages/LoginPage";
// import Registration from "../pages/Registration";
// import Logo from "../assets/img/cosmechick.png";
// import { useNavigate } from "react-router-dom";

class Navbar extends React.Component {
  handleLogout = () => {
    localStorage.removeItem('isAuthenticated');
    window.location.reload();
  };

  render() {
    const navLink = () => {
      const isAuthenticated = localStorage.getItem('isAuthenticated');

      if (isAuthenticated) {
        return (
          <div className='nav-after'>
            <Link to='/MyCart'>
              <img src={cart} alt='cart' className='nav-cart' />
            </Link>

            <img src={notif} alt='notip' className='nav-notif' />

            <img
              src={profile}
              alt='profil'
              type='button'
              className='nav-profile'
              data-bs-toggle='modal'
              data-bs-target='#staticBackdrop'
            />

            <button onClick={this.handleLogout} className='logout ms-4'>
              Logout
            </button>

            <div
              className='modal fade'
              id='staticBackdrop'
              data-bs-backdrop='static'
              data-bs-keyboard='false'
              tabIndex='-1'
              aria-labelledby='staticBackdropLabel'
              aria-hidden='true'
            >
              <div className='modal-dialog modal-lg modal-dialog-centered'>
                <div className='modal-content'>
                  <div className='modal-header'>
                    <h5 className='modal-title' id='staticBackdropLabel'>
                      Profile
                    </h5>
                    <button
                      type='button'
                      className='btn-close'
                      data-bs-dismiss='modal'
                      aria-label='Close'
                    ></button>
                  </div>
                  <div className='modal-body'>
                    <h3>Biodata</h3>
                    <h5>Name : Nandaffa</h5>
                    <h5>Email : nandaffstr1234@gmail.com</h5>
                    <h5>Phone : 087884000077</h5>
                    <h5>Address : Pejaten Timur, Pasar Minggu</h5>
                  </div>
                  <div className='modal-footer'>
                    <button
                      type='button'
                      className='btn btn-secondary'
                      data-bs-dismiss='modal'
                    >
                      Close
                    </button>
                    <button type='button' className='btn btn-primary'>
                      Understood
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      } else {
        return (
          <div className='d-flex flex-row text-center justify-content-center align-items-center nav-before'>
            <Link to='/loginpage' className='nav-login'>
              Login
            </Link>

            <Link to='/registration' className='nav-register text-white'>
              Register
            </Link>
          </div>
        );
      }
    };

    return (
      <nav className='navbar'>
        <div className='nav-fluid'>
          <Link to='/'>
            <button className='navbar-logo'>
              <img alt='truck' src={require('../assets/img/cosmechick.png')} />
            </button>
          </Link>
        </div>
        <div className='navbar-search'>
          <div className='input-group search-input'>
            <input
              type='text'
              className='form-control'
              aria-label='try me'
              aria-describedby='btnGroupAddon'
            />
            <div className='input-group-text' id='btnGroupAddon'>
              Search
            </div>
          </div>
        </div>
        <div className='navbar-nav'>
          {/* <div className="navbar-icon"> */}
          {navLink()}
          {/* </div> */}
        </div>
      </nav>
    );
  }
}

// const NavbarNavigate = () => {
//   const navigate = useNavigate();
//   return <Navbar navigate={navigate} />;
// };

export default Navbar;
