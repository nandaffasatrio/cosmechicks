import Navbar from "./Navbar";
// import TopNav from "./TopNav";

const Header = () => {
  return (
    <div className="Header">
      <Navbar />
    </div>
  );
};

export default Header;
