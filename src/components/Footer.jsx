import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <div className="row footer-wrapper container mx-auto d-flex flex-row">
          <div className="col-xxl-3 col-xl-3 col-lg-3 text-center mt-3 d-flex d-xxl-block d-xl-block d-none flex-row row footer__address">
            <p className="footer-title">Cosmechick</p>
            <hr />
            <div className="col-xxl-4 col-xl-6 col-lg-12 col-md-6 text-center col-6 footer-text">
              <p>Skin is the main thing in our appearance, therefore we must pay attention to it and also take care of it.</p>
            </div>
            <div className="col-4 col-xl-6 col-lg-12 col-md-6 col-sm-12 col-6 footer-text text-center">
              <p>+62 8788 40000 77</p>
              <p>send-me-text@cosmechick.com</p>
            </div>
          </div>
          <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 d-xl-block d-none mt-3 footer__shop">
            <div className="row footer-row container mx-auto d-flex flex-column">
              <div className="col-2">
                {" "}
                <p>Shop</p>
              </div>
              <div className="col-2">
                {" "}
                <p>product</p>
              </div>
              <div className="col-2">
                {" "}
                <p>collection</p>
              </div>
              <div className="col-2">
                {" "}
                <p>
                  {" "}
                  <b> cosmechick</b>
                </p>
              </div>
              <div className="col-2">
                {" "}
                <p>company</p>
              </div>
              <div className="col-2">
                {" "}
                <p>blog</p>
              </div>
            </div>
          </div>
          <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 d-xl-block d-none mt-3">
            <div className="row footer-row container mx-auto d-flex flex-column">
              <div className="col-2 footer-col">
                <p>Serum</p>
              </div>
              <div className="col-2 footer-col">
                <p>Face Wash</p>
              </div>
              <div className="col-2 footer-col ">
                <p>Parfum</p>
              </div>
              <div className="col-2 footer-col">
                <p>Beard Oil</p>
              </div>
              <div className="col-2 footer-col">
                <p>Cream</p>
              </div>
              <div className="col-2 footer-col">
                <p>Make Up</p>
              </div>
            </div>
          </div>
          <div className="col-xl-3 col-xxl-3 col-md-12 col-lg-12 col-sm-12 mt-3 footer__socmed">
            <div className="row footer-row container mx-auto d-flex flex-column">
              <div className="col-12 footer-col text-center">
                <p>
                  {" "}
                  <b> Social Media</b>
                </p>
              </div>
              <div className="col-4 text-center footer-col">
                <div className="row">
                  <div className="col-3">
                    <i className="bi bi-facebook"></i>
                  </div>
                  <div className="col-3">
                    <i className="bi bi-instagram"></i>
                  </div>
                  <div className="col-3">
                    <i className="bi bi-twitter"></i>
                  </div>
                  <div className="col-3">
                    <i className="bi bi-youtube"></i>
                  </div>
                </div>
                <hr />
                <div className="input-group mb-3">
                  <input type="text" className="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                  <span className="subs input-group-text" id="basic-addon2">
                    Subs
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-3 row">
          <div className="col-4">
            <hr />
          </div>
          <div className="col-4 text-center">
            <p> &#169; Copyright Cosmechick 2021</p>
          </div>
          <div className="col-4">
            <hr />
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
