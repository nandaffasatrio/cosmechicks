import { Link } from "react-router-dom";

const TopNav = () => {
  return (
    <div className="topnav">
      <div className="container">
        <ul>
          <li>
            <Link to="/LoginPage">Login</Link>
          </li>
          <li>
            <Link to="/Registration">Register</Link>
          </li>
          <li>
            <Link to="/ProductLanding">Our Product</Link>
          </li>
          <li>
            <Link to="/">Sponsored Content</Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default TopNav;
